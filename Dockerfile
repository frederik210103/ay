FROM python:3.7.0-alpine3.8
RUN pip install pipenv
RUN mkdir ay
ADD . ay
WORKDIR ay
RUN pipenv sync
CMD ["pipenv", "run", "python", "ay.py"]
