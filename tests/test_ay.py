import pytest
from flask_testing import TestCase
from app import app

class Ay(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        app.config['LIVESERVER_TIMEOUT'] = 10
        return app


class RootEndpoint(Ay):
    def test_root(self):
        response = self.client.get('/index')
        self.assert200(response)

    def test_hype(self):
        response = self.client.get('/hypeness')
        self.assert200(response)

    def test_wasted(self):
        response = self.client.get('/wastedness')
        self.assert200(response)

    def test_login(self):
        response = self.client.get('/login')
        self.assert200(response)

