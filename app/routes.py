from flask import url_for, render_template, flash, redirect, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from app import app, db
from app.forms import LoginForm, RegistrationForm, WritePostForm
from app.models import User, Post


@app.route('/')
@app.route('/index')
@login_required
def index():
    posts = Post.query.all()
    return render_template('index.html', title="Homeness", posts=posts[::-1])


@login_required
@app.route("/hypeness")
def hype():

    return render_template("hype.html", title="Hypeness")


@login_required
@app.route("/wastedness")
def wasted():

    return render_template("wasted.html", title="Wastedness")


@login_required
@app.route("/write_post", methods=['GET', 'POST'])
def write_post():
    form = WritePostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, body=form.text.data, author=current_user, public=form.make_public.data)
        db.session.add(post)
        db.session.commit()
        flash("Post sent")
        return redirect(url_for("write_post"))
    return render_template("write_post.html", title="Write a post", form=form)


@login_required
@app.route("/my_posts")
def my_posts():
    posts = current_user.posts.all()
    return render_template("my_posts.html", posts=posts[::-1])


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid holy sequence or holy secret sequence')
            return redirect(url_for("login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("index")
        return redirect(url_for("index"))
    return render_template('login.html', title='Enter your holy profile', form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a part of Ayness!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)
