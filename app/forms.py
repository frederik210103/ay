from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, TextField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User, Post


class LoginForm(FlaskForm):
    username = StringField('Holy sequence', validators=[DataRequired()])
    password = PasswordField('Holy secret sequence', validators=[DataRequired()])
    remember_me = BooleanField('Do you remember?! (Remember me)')
    submit = SubmitField('Enter your holy profile')


class RegistrationForm(FlaskForm):
    username = StringField('Holy sequence', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Holy secret sequence', validators=[DataRequired()])
    password2 = PasswordField('Repeat Holy secret sequence', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different Holy sequence.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class WritePostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    text = TextAreaField("Content", validators=[DataRequired()])
    send = SubmitField("Send")
    make_public = BooleanField('Make public')
